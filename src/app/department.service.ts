import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DepartmentService {
  constructor(private httpClient: HttpClient) {}

  getDepartments() {
    console.log('getting departments from service...');
    return this.httpClient.get(
      'https://run.mocky.io/v3/872d5f24-f0dc-4412-a503-570774f67f4e'
    );
  }

  getCompanies() {
    console.log('getting parameters in department service...');
    return this.httpClient.get(
      'https://run.mocky.io/v3/a7d1efe3-d149-4a15-9746-0b24c0251679'
    );
  }
}
export interface Department {
  id: number;
  name: string;
  description: string;
}
