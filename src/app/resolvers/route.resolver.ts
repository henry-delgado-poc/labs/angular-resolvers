import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { empty, forkJoin, of } from 'rxjs';

import { DepartmentService } from '../department.service';
import { Injectable } from '@angular/core';

@Injectable()
export class RouteResolver implements Resolve<any> {
  /**
   *
   */
  constructor(private departmentService: DepartmentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log('route resolve...');

    return this.departmentService.getDepartments().pipe(
      catchError((error) => {
        return of(null);
      })
    );

    // return forkJoin(
    //   this.departmentService.getDepartments(),
    //   this.departmentService.getCompanies()
    // ).pipe(
    //   map((data) => {
    //     console.log('departments', data[0]);
    //     console.log('companies', data[1]);
    //     return {
    //       departments: data[0],
    //       companies: data[1],
    //     };
    //   })
    // );
  }
}
