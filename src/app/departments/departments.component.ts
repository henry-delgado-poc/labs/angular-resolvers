import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Department } from '../department.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css'],
})
export class DepartmentsComponent implements OnInit {
  departments: Department[] = [];
  constructor(private activatedRoute: ActivatedRoute) {
    console.log('constructor from department component...');
  }

  ngOnInit(): void {
    console.log('on init from department components...');
    this.activatedRoute.data.subscribe((data) => {
      console.log('inside department resolved data...');
      this.departments = data as Department[];
      console.log(this.departments);
    });
  }
}
