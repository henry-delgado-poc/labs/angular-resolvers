import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DepartmentService } from './department.service';
import { DepartmentsComponent } from './departments/departments.component';
import { NgModule } from '@angular/core';
import { RouteResolver } from './resolvers/route.resolver';

const routes: Routes = [
  {
    path: 'dashboard',
    pathMatch: 'full',
    component: DashboardComponent,
  },
  {
    path: 'departments',
    pathMatch: 'full',
    component: DepartmentsComponent,
    resolve: {
      data: RouteResolver,
    },
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard',
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [RouteResolver],
})
export class AppRoutingModule {}
