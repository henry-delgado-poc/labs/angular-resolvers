# Angular 12 Resolvers

The purpose of this lab is to practive Angular Resolvers using Angular 12.

Resolvers are useful in cases where we need to retrieve data and show a specific section from that set.
Resolvers allows us to load the data before we navigate to start using that data.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Steps

Simplified steps for demo purposes.

1. Create a folder for your resolvers. I would place these within your modules in real life.
2. Create route.resolver.ts. In real life, you could name for example contacts.resolver.ts
3. Create some components: Dashboard and Departments
4. Create a service for loading the departments
5. Configure the app-routing.module.ts with new components
6. Add router-outlet to app component.
7. Add the resolver on the routes section containining the department component.

Using [mocky](https://designer.mocky.io/) to design an API for testing purpose.
